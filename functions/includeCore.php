<?php
function includeUrl($defaultInclude) {
    $geturl = strip_tags(trim(filter_input(INPUT_GET,'url', FILTER_DEFAULT)));
    $seturl = (empty($geturl)) ? $defaultInclude : $geturl;
    $url = explode('/',$seturl);
    $url[1] = (empty($url[1]) ? null : $url[1]);

    if(file_exists(INCLUDE_PATH . $url[0] . '.php')){
        require INCLUDE_PATH . $url[0] . '.php';
    } else if(file_exists(INCLUDE_PATH . $url[0] . '/' . $url[1] . '.php')){
            require INCLUDE_PATH . $url[0] . '/' . $url[1] . '.php';
    } else {require INCLUDE_PATH . '404.php';}
    
    if(in_array("index", $url)){
        require INCLUDE_PATH . '404.php';
    }
}
?>
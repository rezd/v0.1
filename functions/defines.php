<?php
header('Content-type: text/html; charset=utf-8');    
setlocale(LC_ALL, NULL);
setlocale(LC_ALL, 'pt_BR.utf-8');

define( 'DS', '/');
// define('ROOT_URL','http://localhost:8080/');

define( "URL" , $_SERVER['REQUEST_URI']);
define( "INCLUDE_PATH" , $_SERVER['DOCUMENT_ROOT'] . "pages/");


define('LIB_TMPL',URL.'_tmpl'		.DS);
define('LIB_MDEL',URL.'models'		.DS);
define('LIB_FUNC',URL.'functions'	.DS);
define('LIB_ACTS',URL.'actions'		.DS);
define('LIB_HELP',URL.'helpers'		.DS);
?>